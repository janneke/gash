;;; Gash -- Guile As SHell
;;; Copyright © 2018 Timothy Sample <samplet@ngyro.com>
;;;
;;; This file is part of Gash.
;;;
;;; Gash is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; Gash is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Gash.  If not, see <http://www.gnu.org/licenses/>.

(define-module (gash built-ins break)
  #:use-module (gash built-ins utils)
  #:use-module (gash compat)
  #:use-module (gash environment)
  #:use-module (ice-9 match))

;;; Commentary:
;;;
;;; The 'break' utility.
;;;
;;; Code:

(define (main . args)
  (match args
    (() (main "1"))
    ((arg)
     (match (string->positive-integer arg)
       (#f (format (current-error-port)
                   "gash: break: argument must be a positive integer~%")
           (throw 'shell-error))
       (n (set-status! 0)
          (sh:break (1- n))
          (format (current-error-port)
                  "gash: break: no loop to break from~%")
          EXIT_SUCCESS)))
    (_ (format (current-error-port)
               "gash: break: too many arguments~%")
       (throw 'shell-error))))
