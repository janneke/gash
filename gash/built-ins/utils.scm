;;; Gash -- Guile As SHell
;;; Copyright © 2018, 2019 Timothy Sample <samplet@ngyro.com>
;;;
;;; This file is part of Gash.
;;;
;;; Gash is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; Gash is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Gash.  If not, see <http://www.gnu.org/licenses/>.

(define-module (gash built-ins utils)
  #:use-module (gash compat)
  #:use-module (ice-9 match)
  #:export (get-evaluator
            built-in?
            split-assignment
            string->positive-integer
            string->nonnegative-integer
            string->exit-status))

;;; Commentary:
;;;
;;; Utility functions shared by built-ins.
;;;
;;; Code:

;; We need to be able to run Shell code from the 'dot' and 'eval'
;; built-ins.  This is a bit of trickery to avoid a module dependency
;; loop.
(define (get-evaluator)
  (module-ref (resolve-interface '(gash eval)) 'eval-sh))

;; Similar to 'get-evaluator', we avoid a dependency loop by looking
;; up functions dynamically.
(define (built-in? name)
  (let ((search-special-built-ins
         (module-ref (resolve-interface '(gash built-ins))
                     'search-special-built-ins))
        (search-built-ins
         (module-ref (resolve-interface '(gash built-ins))
                     'search-built-ins)))
    (or (search-special-built-ins name)
        (search-built-ins name))))

(define (split-assignment assignment)
  (match (string-index assignment #\=)
    (#f (values assignment #f))
    (index (let ((name (substring assignment 0 index)))
             (match (substring assignment (1+ index))
               ((? string-null?) (values name #f))
               (value (values name value)))))))

(define char-set:ascii-digit
  (char-set-intersection char-set:ascii char-set:digit))

(define (string->positive-integer s)
  "Return the positive integer represented by the string @var{s}.  If
@var{s} does not represent a positive, decimal integer return
@code{#f}."
  (and=> (and (string-every char-set:ascii-digit s) (string->number s))
         (lambda (n)
           (and (exact-integer? n) (> n 0) n))))

(define (string->nonnegative-integer s)
  "Return the nonnegative integer represented by the string @var{s}.
If @var{s} does not represent a nonnegative, decimal integer return
@code{#f}."
  (and=> (and (string-every char-set:ascii-digit s) (string->number s))
         (lambda (n)
           (and (exact-integer? n) (>= n 0) n))))

(define (string->exit-status s)
  "Return the exit status represented by the string @var{s}.  If
@var{s} does not represent an exit status (a decimal integer from 0 to
255) return @code{#f}."
  (and=> (and (string-every char-set:ascii-digit s) (string->number s))
         (lambda (n)
           (and (exact-integer? n) (>= n 0) (<= n 255) n))))
