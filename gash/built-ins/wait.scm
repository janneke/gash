;;; Gash -- Guile As SHell
;;; Copyright © 2020 Timothy Sample <samplet@ngyro.com>
;;;
;;; This file is part of Gash.
;;;
;;; Gash is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; Gash is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Gash.  If not, see <http://www.gnu.org/licenses/>.

(define-module (gash built-ins wait)
  #:use-module (gash built-ins utils)
  #:use-module (gash compat)
  #:use-module (ice-9 match))

;;; Commentary:
;;;
;;; The 'wait' utility.
;;;
;;; Code:

(define (main . args)
  (let ((pids (map string->nonnegative-integer args)))
    (match pids
      ((pid)
       (match (false-if-exception (waitpid pid))
         (#f 127)
         ((_ . status) (status:exit-val status))))
      (_ (format (current-error-port)
                 "~a: wait: Invalid arguments."
                 (car (program-arguments)))
         EXIT_FAILURE))))
