;;; Gash -- Guile As SHell
;;; Copyright © 2019 Timothy Sample <samplet@ngyro.com>
;;; Copyright © 2019 Jan (janneke) Nieuwenhuizen <janneke@gnu.org>
;;;
;;; This file is part of Gash.
;;;
;;; Gash is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; Gash is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Gash.  If not, see <http://www.gnu.org/licenses/>.

(define-module (gash compat textual-ports)
  #:use-module (gash compat))

;;; Commentary:
;;;
;;; This module provides is a very simple Guile 2.0 shim for
;;; '(ice-9 textual-ports)'.
;;;
;;; Code:

(if-guile-version-below (2 2 0)
  (begin
    (use-modules (rnrs io ports))
    (re-export get-char
               get-line
               get-string-all
               put-char
               lookahead-char)
    (define-public (unget-char port char)
      (unread-char char port)))
  (begin
    (use-modules (ice-9 textual-ports))
    (re-export get-char
               get-line
               get-string-all
               lookahead-char
               put-char
               unget-char)))
