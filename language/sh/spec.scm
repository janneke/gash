;;; Gash --- Guile As SHell
;;; Copyright © 2020 Stephen J. Scheck <sscheck@singularsyntax.one>
;;;
;;; This file is part of Gash.
;;;
;;; Gash is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; Gash is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Gash.  If not, see <http://www.gnu.org/licenses/>.

(define-module (language sh spec)
  #:use-module (gash environment)
  #:use-module (gash eval)
  #:use-module (gash parser)
  #:use-module (system base language)
  #:export (sh))

;;; Commentary:
;;;
;;; This module contains the language spec definition that extends
;;; Guile allowing use of shell syntax from the REPL by invocation
;;; of the ,language meta-command.
;;;
;;; Code:

(define-language sh
  #:title "Guile as Shell"
  #:reader (lambda (port env) (read-sh port))
  #:evaluator (lambda (x module) (eval-sh x) (get-status))
  #:printer write)
