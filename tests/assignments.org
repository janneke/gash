;;; Gash -- Guile As SHell
;;; Copyright © 2016, 2017, 2018 R.E.W. van Beusekom <rutger.van.beusekom@gmail.com>
;;; Copyright © 2018, 2019 Jan (janneke) Nieuwenhuizen <janneke@gnu.org>
;;; Copyright © 2019 Timothy Sample <samplet@ngyro.com>
;;;
;;; This file is part of Gash.
;;;
;;; Gash is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; Gash is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Gash.  If not, see <http://www.gnu.org/licenses/>.

* assignment-backtick
:script:
#+begin_src sh
  obj=ar.o
  objs="$objs `basename $obj`"
  echo "objs:>$objs<"
#+end_src
:stdout:
#+begin_example
  objs:> ar.o<
#+end_example

* assignment-doublequoted-doublequotes
:script:
#+begin_src sh
  aliaspath=alias
  localedir=locale
  defines="-DALIASPATH=\"${aliaspath}\" -DLOCALEDIR=\"${localedir}\""
  echo cc $defines
#+end_src
:stdout:
#+begin_example
  cc -DALIASPATH="alias" -DLOCALEDIR="locale"
#+end_example

* assignment-double-quote
:script:
#+begin_src sh
  srcdir="."
#+end_src

* assignment-echo
:script:
#+begin_src sh
  SHELL=/bin/bash
  echo $SHELL
#+end_src
:stdout:
#+begin_example
  /bin/bash
#+end_example

* assignment-empty
:script:
#+begin_src sh
  a=
  echo a:$a
#+end_src
:stdout:
#+begin_example
  a:
#+end_example

* assignment
:script:
#+begin_src sh
  SHELL=/bin/bash
#+end_src

* assignment-singlequote
:script:
#+begin_src sh
  srcdir='.'
#+end_src

* assignment-variable-word
:script:
#+begin_src sh
  SHELL=gash
  bin=${SHELL}/bin
  echo $bin
#+end_src
:stdout:
#+begin_example
  gash/bin
#+end_example

* assignment-word-variable
:script:
#+begin_src sh
  SHELL=gash
  PATH=bin:${SHELL}
  echo $PATH
#+end_src
:stdout:
#+begin_example
  bin:gash
#+end_example

* Assignments reset exit status
:script:
#+begin_src sh
  set +e
  false
  x=foobar
#+end_src

* Assigning exit status works
:script:
#+begin_src sh
  set +e
  false
  x=$?
  echo $x
#+end_src
:stdout:
#+begin_example
  1
#+end_example

* Assignments use command substitutions for exit status
:script:
#+begin_src sh
  set +e
  x=$(false)
  echo $?
  x=$(true)
  echo $?
#+end_src
:stdout:
#+begin_example
  1
  0
#+end_example

* Assignments update exit status on the fly
:script:
#+begin_src sh
  set +e
  false
  x=$? y=$(true) z=$?
  echo $x $z
#+end_src
:stdout:
#+begin_example
  1 0
#+end_example
