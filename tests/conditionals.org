;;; Gash -- Guile As SHell
;;; Copyright © 2016, 2017, 2018 R.E.W. van Beusekom <rutger.van.beusekom@gmail.com>
;;; Copyright © 2018, 2019 Jan (janneke) Nieuwenhuizen <janneke@gnu.org>
;;; Copyright © 2019 Timothy Sample <samplet@ngyro.com>
;;;
;;; This file is part of Gash.
;;;
;;; Gash is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; Gash is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Gash.  If not, see <http://www.gnu.org/licenses/>.

* else-multiple
:script:
#+begin_src sh
  if false; then
      :
  else
      echo one
      echo two
  fi
#+end_src

* if-bracket-false
:script:
#+begin_src sh
  if \[ 0 = 1 ]; then
      exit 1
  fi
  exit 0
#+end_src

* if-bracket
:script:
#+begin_src sh
  if \[ 1 = 1 ]; then
      exit 0
  fi
  exit 1
#+end_src

* if-elif
:script:
#+begin_src sh
  if false; then
      exit 1
  elif false; then
       exit 2
  else
      exit 0
  fi
  exit 1
#+end_src

* if-else
:script:
#+begin_src sh
  if false; then
      exit 1
  else
      exit 0
  fi
  exit 1
#+end_src

* if-false
:script:
#+begin_src sh
  if false; then
      exit 1
  fi
  exit 0
#+end_src

* if-line
:script:
#+begin_src sh
  if true; then echo yay; fi
#+end_src

* if-multiple
:script:
#+begin_src sh
  if true; then
      echo one
      echo two
  fi
#+end_src
:stdout:
#+begin_example
  one
  two
#+end_example

* if-redirect
:script:
#+begin_src sh
  if $SHELL --version | grep foobar 2>/dev/null; then
      exit 1
  else
      exit 0
  fi
#+end_src

* if
:script:
#+begin_src sh
  if true; then
      exit 0
  fi
  exit 1
#+end_src

* if-test-false
:script:
#+begin_src sh
  if test 0 = 1; then
      exit 1
  fi
  exit 0
#+end_src

* if-test
:script:
#+begin_src sh
  if test 1 = 1; then
      exit 0
  fi
  exit 1
#+end_src

* if-word-variable
:script:
#+begin_src sh
  if \[ x"$y" = x ]; then
      exit 0
  fi
  exit 1
#+end_src
