;;; Gash -- Guile As SHell
;;; Copyright © 2016, 2017, 2018 R.E.W. van Beusekom <rutger.van.beusekom@gmail.com>
;;; Copyright © 2018, 2019 Jan (janneke) Nieuwenhuizen <janneke@gnu.org>
;;; Copyright © 2019 Timothy Sample <samplet@ngyro.com>
;;;
;;; This file is part of Gash.
;;;
;;; Gash is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; Gash is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Gash.  If not, see <http://www.gnu.org/licenses/>.

* exit-0
:script:
#+begin_src sh
  exit 0
#+end_src

* exit-1
:script:
#+begin_src sh
  exit 1
#+end_src
:status: 1

* exit-2
:script:
#+begin_src sh
  exit 2
#+end_src
:status: 2

* exit-error
:script:
#+begin_src sh
  set +e
  ls /foo
#+end_src
:status: 2

* exit
:script:
#+begin_src sh
  exit
#+end_src

* exit-var
:script:
#+begin_src sh
  set +e
  ls /foo
  exit $?
#+end_src
:status: 2
