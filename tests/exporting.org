;;; Gash -- Guile As SHell
;;; Copyright © 2016, 2017, 2018 R.E.W. van Beusekom <rutger.van.beusekom@gmail.com>
;;; Copyright © 2018, 2019 Jan (janneke) Nieuwenhuizen <janneke@gnu.org>
;;; Copyright © 2019 Timothy Sample <samplet@ngyro.com>
;;;
;;; This file is part of Gash.
;;;
;;; Gash is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; Gash is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Gash.  If not, see <http://www.gnu.org/licenses/>.

* export-new
:script:
#+begin_src sh
  export V
  
  if [ -n "$V" ]; then
      echo VEE
  else
      echo no VEE
  fi
#+end_src
:stdout:
#+begin_example
  no VEE
#+end_example

* sh-export-new
:script:
#+begin_src sh
  export V
  
  . tests/data/42-export-new.sh
#+end_src
:stdout:
#+begin_example
  no VEE
#+end_example

* sh-export
:script:
#+begin_src sh
  bar=baz
  export baz
  sh tests/data/script.sh
#+end_src
:stdout:
#+begin_example
  foo:bar
  bar:
#+end_example

* sh
:script:
#+begin_src sh
  sh tests/data/script.sh
  echo $foo
#+end_src
:stdout:
#+begin_example
  foo:bar
  bar:
  
#+end_example
