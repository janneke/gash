;;; Gash -- Guile As SHell
;;; Copyright © 2019 Timothy Sample <samplet@ngyro.com>
;;;
;;; This file is part of Gash.
;;;
;;; Gash is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; Gash is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Gash.  If not, see <http://www.gnu.org/licenses/>.


;; Special built-ins

* Assignments escape special built-ins
:script:
#+begin_src sh
  eval 'X=foo'
  echo $X
#+end_src
:stdout:
#+begin_example
  foo
#+end_example

* Temporary assignments escape special built-ins
:script:
#+begin_src sh
  X=bar eval 'X=foo'
  echo $X
#+end_src
:stdout:
#+begin_example
  foo
#+end_example

* Updates of temporary assignments escape special built-ins
:script:
#+begin_src sh
  X=foo
  X=bar eval 'X=baz'
  echo $X
#+end_src
:stdout:
#+begin_example
  baz
#+end_example

* Temporary assignments are not exported in special built-ins
:script:
#+begin_src sh
  X=foo eval 'guile -c '\''(format #t "~a~%" (getenv "X"))'\'
#+end_src
:stdout:
#+begin_example
  #f
#+end_example

* Temporary exports do not escape special built-ins
:script:
#+begin_src sh
  X=bar
  X=foo :
  guile -c '(format #t "~a~%" (getenv "X"))'
#+end_src
:stdout:
#+begin_example
  #f
#+end_example

* Exports escape special built-ins
:script:
#+begin_src sh
  X=foo
  eval 'export X'
  guile -c '(format #t "~a~%" (getenv "X"))'
#+end_src
:stdout:
#+begin_example
  foo
#+end_example

* Exports of temporary assignments escape special built-ins
:script:
#+begin_src sh
  X=foo eval 'export X'
  guile -c '(format #t "~a~%" (getenv "X"))'
#+end_src
:stdout:
#+begin_example
  foo
#+end_example


;; Functions

* Assignments escape functions
:script:
#+begin_src sh
  f() {
      X=foo
  }
  f
  echo $X
#+end_src
:stdout:
#+begin_example
  foo
#+end_example

* Temporary assignments do not escape functions
:script:
#+begin_src sh
  f() {
      X=foo
  }
  X=bar f
  echo $X
#+end_src
:stdout:
#+begin_example
  
#+end_example

* Updates of temporary assignments do not escape functions
:script:
#+begin_src sh
  f() {
      X=baz
  }
  X=foo
  X=bar f
  echo $X
#+end_src
:stdout:
#+begin_example
  foo
#+end_example

* Temporary assignments are not exported in functions
:script:
#+begin_src sh
  f() {
      guile -c '(format #t "~a~%" (getenv "X"))'
  }
  X=foo f
#+end_src
:stdout:
#+begin_example
  #f
#+end_example

* Temporary exports do not escape functions
:script:
#+begin_src sh
  f() {
      :
  }
  X=bar
  X=foo f
  guile -c '(format #t "~a~%" (getenv "X"))'
#+end_src
:stdout:
#+begin_example
  #f
#+end_example

* Exports escape functions
:script:
#+begin_src sh
  f() {
      export X
  }
  X=foo
  f
  guile -c '(format #t "~a~%" (getenv "X"))'
#+end_src
:stdout:
#+begin_example
  foo
#+end_example

* Exports of temporary assignments do not escape functions
:script:
#+begin_src sh
  f() {
      export X
  }
  X=foo f
  guile -c '(format #t "~a~%" (getenv "X"))'
#+end_src
:stdout:
#+begin_example
  #f
#+end_example

* Read-only temporary assignments do not escape functions
:script:
#+begin_src sh
  f() {
      readonly X
  }
  X=foo f
  echo $X
#+end_src
:stdout:
#+begin_example
  
#+end_example
